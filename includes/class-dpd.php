<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://www.dachcom.com
 * @since      1.0.0
 *
 * @package    Dachcom_Page_Designer
 * @subpackage Dachcom_Page_Designer/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Dachcom_Page_Designer
 * @subpackage Dachcom_Page_Designer/includes
 * @author     Stefan Hagspiel <shagspiel@dachcom.ch>
 */
class Dachcom_Page_Designer {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Dachcom_Page_Designer_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'dachcom-page-designer';
		$this->version = '1.8.2';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Dachcom_Page_Designer_Loader. Orchestrates the hooks of the plugin.
	 * - Dachcom_Page_Designer_i18n. Defines internationalization functionality.
	 * - Dachcom_Page_Designer_Admin. Defines all hooks for the admin area.
	 * - Dachcom_Page_Designer_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-dpd-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-dpd-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-dpd-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-dpd-public.php';

        /**
         * Load the Public Functions (API)
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-dpd-functions.php';

        /**
         * Get the form settings Class
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-dpd-settings.php';

        /**
         * Load the Module Loader
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/acf/class-module-loader.php';

        /**
         * Load the Module Factory
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/acf/class-module-factory.php';

        /**
         * Load the Module Config
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/acf/core/Config.php';

        /**
         * Load the Module Controller
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/acf/core/Controller.php';

        /**
         * Load the Module Model
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/acf/core/Model.php';

        /**
         * Load the View Renderer
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-dpd-view-renderer.php';

        $this->loader = new Dachcom_Page_Designer_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Dachcom_Page_Designer_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Dachcom_Page_Designer_i18n();
		$plugin_i18n->set_domain( $this->get_plugin_name() );

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Dachcom_Page_Designer_Admin( $this->get_plugin_name(), $this->get_version() );

        if( defined('USE_DC_PLUGIN_OBSERVER' ) && USE_DC_PLUGIN_OBSERVER === TRUE ) {

            $this->loader->add_action( 'init', 					    $plugin_admin, 'register_plugin' );

        } else {

            $this->loader->add_action( 'admin_menu',                $plugin_admin, 'add_admin_menu' );

        }

        $this->loader->add_action( 'admin_init',			        $plugin_admin, 'plugin_core_settings_api_init' );

        $this->loader->add_action( 'acf/include_fields',	        $plugin_admin, 'include_acf_data' );
        $this->loader->add_action( 'acf/input/admin_head',	        $plugin_admin, 'on_rendered_fields' );
        $this->loader->add_action( 'acf/fields/wysiwyg/toolbars',	$plugin_admin, 'update_wysiwyg_toolbar' );

    }

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Dachcom_Page_Designer_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'init', $plugin_public, 'initialize_module_controller' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Dachcom_Page_Designer_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
