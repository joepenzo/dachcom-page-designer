<?php

class Dachcom_Page_Designer_ModuleFactory {

    var $repository = NULL;

    /**
     *
     * Registers a Module to Factory.
     * Loads a Config, Controller and Model file.
     *
     * @param $moduleName
     * @param $modulePath
     * @param $usingThemosis
     *
     * @return \PageDesignerController
     * @throws
     */
    public function registerModule( $moduleName, $modulePath, $usingThemosis) {

        $viewPath = NULL;

        if( !is_dir( $modulePath ) ) {

            throw new Exception( $moduleName . ' in ' . $modulePath . ' not Found');

        }

        $moduleContent = scandir( $modulePath );

        if( empty( $moduleContent ) )
            return false;

        $configFile = NULL;
        $controllerFile = NULL;
        $modelFile = NULL;

        if( is_file( $modulePath . '/Config/' . $moduleName . '.php' ) ) {

            $configFile = $modulePath . '/Config/' . $moduleName . '.php';
            include_once( $configFile );

        }

        if( is_file( $modulePath . '/Controller/' . $moduleName . '.php' ) ) {

            $controllerFile = $modulePath . '/Controller/' . $moduleName . '.php';
            include_once( $controllerFile );

        }

        if( is_file( $modulePath . '/Model/' . $moduleName . '.php' ) ) {

            $modelFile = $modulePath . '/Model/' . $moduleName . '.php';
            include_once( $modelFile );

        }

        if( is_null( $configFile ) || is_null( $controllerFile ) )
            return FALSE;

        if( !$usingThemosis ) {

            $viewPath = $modulePath . '/View/';

        }

        $controllerName     = "{$moduleName}Controller";
        $configName         = "{$moduleName}Config";
        $modelName          = "{$moduleName}Model";

        /**
         * @var PageDesignerController $moduleController
         */
        $moduleController = new $controllerName();

        /**
         * @var PageDesignerConfig $moduleConfig
         */
        $moduleConfig = new $configName();
        $moduleConfig->setViewPath( $viewPath );

        /** Add Config */
        $moduleController->addConfig( $moduleConfig );

        /** Add Model */
        if( !is_null( $modelFile ) ) {

            /**
             * @var PageDesignerModel $moduleModel
             */
            $moduleModel= new $modelName();

            add_action('dachcom-page-designer/before-render/' . strtolower($moduleName) . '_module', array($moduleModel, 'beforeRender') );

            $moduleController->addModel( $moduleModel );

        }

        /** Add Actions */
        $moduleController->addActions();

        $this->addToRepository( $moduleName, $moduleController );

        return $moduleController;

    }

    private function addToRepository( $moduleName, $moduleController ) {

        $this->repository[ $moduleName ] = $moduleController;

        return TRUE;

    }

    public function getModuleInfo( $moduleName ) {

        if( !isset( $this->repository[ $moduleName ]  ) ) {

            return FALSE;

        }

        return $this->repository[ $moduleName ];

    }

}