��             +         �  
   �  
   �  	   �  	   �  	   �     �  !        %  #   ;     _     ~     �  >   �     �  }         ~     �  (   �     �  !   �  #     �   ,  !   �               %  3   <  (   p  Q   �     �       �    
   �	  
   �	  	   
  	   
  	   
     %
  !   +
     M
     c
     �
     �
     �
  /   �
     �
  h   
     s     �  *   �  
   �      �  #   �  �        �  
   �     �     �  1   �  '   .     V     t     �                                                                                 	      
                                                         10-Spaltig 12-Spaltig 4-Spaltig 6-Spaltig 8-Spaltig Basic DACHCOM DIGITAL : Stefan Hagspiel Dachcom Page Designer Dachcom Page Designer Einstellungen Dachcom Page Designer Settings Dachcom Settings Einstellungen speichern Es wurde keine CSS mit dem Namen "visualizer_%s.css" gefunden. Generates a Page Designer Hinweis: Wenn Sie das Fenster über ESC oder das Schließen-Symbol rechts oben schließen, wird der Inhalt nicht gespeichert. Inhalt bearbeiten Keine Vorschau verfügbar. Mehrere Spalten im 1. Level deaktivieren Modul hinzufügen Name des inneren "Spalten-Feldes" Name des äusseren "Spalten-Feldes" Plugins, welche vom <a href="http://dachcom-digital.ch">Dachcom Digital Team</a> entwickelt wurden, benötigen das "Dachcom Plugin Observer" MU-Plugin. <a href="%s">Zur&uuml;ck</a>. Responsive Adjustierung vornehmen Spalte hinzufügen Spalteninhalt Speichern & Schließen Welche Spaltenbreiten sind auf dem 1. Level erlaub? Welches Spaltenlayout besitzt das Theme? Wenn Sie das 1. Spalten Level deaktivieren möchten, aktivieren sie diese Option. http://www.dachcom.com ohne Plural-Forms: nplurals=2; plural=(n != 1);
Project-Id-Version: Dachcom Page Designer
POT-Creation-Date: 2015-12-03 13:51-0600
PO-Revision-Date: 2015-12-03 13:52-0600
Language-Team: Alec Rippberger <alec@westwerk.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.6
X-Poedit-Basepath: ..
X-Poedit-WPHeader: dachcom-page-designer.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: Alec Rippberger <alec@westwerk.com>
Language: en
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 10-Columns 12-Columns 4-Columns 6-Columns 8-Columns Basic DACHCOM DIGITAL : Stefan Hagspiel Dachcom Page Designer DACHCOM Page Designer Settings Dachcom page designer settings Dachcom Settings Save Settings File named "visualizer_ % s.css" was not found. Generates a Page Designer Note: If you close the window via ESC or the close icon on the top right, the content will not be saved. Edit Content No preview available. Disable multiple columns on the 1st level. Add Module Name of the inner "column field" Name of the external "column field" Plugins , which were developed from this code <a href="http://dachcom-digital.ch"> DACHCOM Digital team need the "DACHCOM Plugin Observer" MU plugin . <a href="%s"> To PIECE code. Make Responsive Add Column Column Content Save & Close Which column widths are allowed on the 1st level? Which column layout does the theme use? Disable the 1st column level. http://www.dachcom.com Without 